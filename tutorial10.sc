def average(list: List[Int]): Int = list.foldLeft(0)(_ + _) / list.size

trait Additive[A] {
  def plus(a: A, b: A): A
  def zero: A
}

object Additive {
  implicit object IntAdditive extends Additive[Int] {
    def plus(a: Int, b: Int): Int = a + b
    def zero: Int = 0
  }
  implicit object DoubleAdditive extends Additive[Double] {
    def plus(a: Double, b: Double): Double = a + b
    def zero: Double = 0.0
  }
}

//def average[A] (lst: List[A])(implicit m: Additive[A]): A = {
//  val length: Int = lst.length
//  val sum: A = lst.foldLeft(m.zero)((x, y) => m.plus(x, y))
//  sum / length
//}

object Nums {
  trait Num[A] {
    def plus(a: A, b:A): A
    def minus(a: A, b: A): A
    def multiply(a: A, b: A): A
    def divide(a: A, b: A): A
    def zero: A
  }
  object Num{
    implicit object IntNum extends Num[Int]{
      def plus(a: Int, b:Int) = a + b
      def minus(a: Int, b: Int): Int = a - b
      def multiply(a: Int, b: Int): Int = a * b
      def divide(a: Int, b: Int): Int = a/b
      def zero: Int = 0 
    }
    implicit object DoubleNum extends Num[Double] {
      def plus(a: Double, b:Double) = a + b
      def minus(a: Double, b: Double): Double = a - b
      def multiply(a: Double, b: Double): Double = a * b
      def divide(a: Double, b: Double): Double = a/b
      def zero: Double = 0.0
    }
  }
}

object FromInts {
  trait FromInt[A] {
    def to(from: Int): A
  }
  object FromInt {

    implicit object FromIntToInt extends FromInt[Int] {
      def to(from: Int): Int = from
    }

    implicit object FromToDouble extends FromInt[Double] {
      def to(from: Int): Double = from
    }

  }
}

import Nums._
import FromInts._

def average[A](lst: List[A])(implicit a: Num[A], b: FromInt[A]):A = {
  val length: Int = lst.length
  val sum: A = lst.foldLeft(a.zero)((x, y) => a.plus(x, y))
  a.divide(sum, b.to(length))
}

average(List(1, 3, 5))
average(List(1.5, 2.5, 3.5))

def median[A: Num: Ordering:FromInt](lst: List[A]): A = {
  val num = implicitly[Num[A]]
  val int = implicitly[FromInt[A]]
  val ord = implicitly[Ordering[A]]
  val size = lst.size
  require(size > 0)
  val sorted = lst.sorted
  if(size % 2 == 1){
    sorted(size / 2)
  }else {
    val fst = sorted((size/ 2) -1)
    val snd = sorted((size / 2))
    num.divide(num.plus(fst, snd), int.to(2))
  }
}

median(List(1,2,3))
median(List(1.5, 2.5, 3.5))



//
//object Serializers {
//  trait Serializer[A] {
//    def serialize(obj: A): String
//  }
//  def string[A:Serializer](obj: A) : String = {
//    implicitly[Serializer[A]].serialize(obj)
//  }
//
//}


object Serializers {
  trait Serializer[A] {
    def serialize(obj: A): String
  }
  def string[A:Serializer](obj: A) : String = {
    implicitly[Serializer[A]].serialize(obj)
  }
  implicit object IntSerializer extends Serializer[Int] {
    def serialize(obj: Int): String = obj.toString
  }

  implicit object StringSerializer extends Serializer[String] {
    def serialize(obj: String): String = obj
  }

}
import Serializers._
string(123)