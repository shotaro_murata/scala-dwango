// Start writing your ScalaFiddle code here
// println("Hello, World!")
println(0xff)
println(1e308)
println(9223372036854775807L)
// println(9223372036854775808L)
// println(9223372036854775807)
println(922337203685477580.7)
println(1.00000000000000000001 == 1)
println("\u3042")
println("\ud842\udf9f")

println(2147483647 + 1)
println(9223372036854775807L + 1)
println(1e308 + 1)
println(1 + 0.0000000000000000001)
println(1 - 1)
println(1 - 0.1)
println(0.1 - 1)
println(0.1 - 0.1)
println(0.0000000000000000001 - 1)
println(0.1 * 0.1)
println(20 * 0.1)
println(1 / 3)
println(1.0 / 3)
println(1 / 3.0)
println(3.0 / 3.0)
println(1.0 / 10 * 1 / 10)
println(1 / 10 * 1 / 10.0)

//���K���
var x : Int = 3950000
val y = 3950000
val rate = 0.023

println((x*rate*2/3).toInt)

var t : Int = 1980000
val s : Int = (26400*100/1.6).toInt
println((t - s + 26400)*100/t)