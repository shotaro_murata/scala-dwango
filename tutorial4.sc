class Cell[A](var value : A) {
  def put(newValue : A) : Unit = {
    value = newValue
  }
  
  def get() : A = value
}

var cell = new Cell[Int](1)

cell.put(2)
println(cell.get())

class Pair[A, B](val a: A, val b: B) {
  override def toString(): String = "(" + a + "," + b + ")"
}

def divide(m : Int, n : Int) : Pair[Int,Int] = new Pair[Int, Int](m / n, m % n)
println(divide(7, 3))

//���K���
trait Stack[+A] {
  def push[E >: A](e: E): Stack[E]
  def top: A
  def pop: Stack[A]
  def isEmpty: Boolean
}

class NonEmptyStack[+A](private val first: A, private val rest: Stack[A]) extends Stack[A] {
  def push[E >: A](e: E): Stack[E] = new NonEmptyStack[E](e, this)
  def top: A = first
  def pop: Stack[A] = rest
  def isEmpty: Boolean = false
}

case object EmptyStack extends Stack[Nothing] {
  def push[E >: Nothing](e: E): Stack[E] = new NonEmptyStack[E](e, this)
  def top: Nothing = throw new IllegalArgumentException("empty stack")
  def pop: Nothing = throw new IllegalArgumentException("empty stack")
  def isEmpty: Boolean = true
}

object Stack {
  def apply(): Stack[Nothing] = EmptyStack
}

val x1: String => AnyRef = (x: AnyRef) => x
println(x1)