// Start writing your ScalaFiddle code here
//���K���
sealed abstract class DayOfWeek
case object Sunday extends DayOfWeek
case object Monday extends DayOfWeek
case object Tuesday extends DayOfWeek
case object Wednesday extends DayOfWeek
case object Thursday extends DayOfWeek
case object Friday extends DayOfWeek
case object Saturday extends DayOfWeek

def nextDayOfWeek(d: DayOfWeek): DayOfWeek = d match{
  case Sunday => Monday
  case Monday => Tuesday
  case Tuesday => Wednesday
  case Wednesday => Thursday
  case Thursday => Friday
  case Friday => Saturday
  case Saturday => Sunday
}

println(nextDayOfWeek(Sunday))

//���K���2
// println(List(1,2,3).max)
sealed abstract class Tree
case class Branch(value: Int, left: Tree, right: Tree) extends Tree
case object Empty extends Tree

val tree: Tree = Branch(1, Branch(2, Empty, Empty), Branch(3, Empty, Empty))

def max(tree: Tree): Int = tree match{
  case Branch(v, l, r) => List(v, max(l), max(r)).max
  case Empty => 0
}

println(max(tree))

def min(tree: Tree): Int = tree match{
  case Branch(v, l, r)  => List(v, min(l), min(r)).min
  case Empty => 0
}

println(min(tree))

def depth(tree: Tree): Int = tree match{
  case Branch(v, l, r) => List(depth(l)+1, depth(r)+1).max
  case Empty => 0
}

println(depth(tree))
println(depth(Empty))
println(depth(Branch(10, Branch(20,
                    Empty,
                    Empty
                 ), Empty)))
println(depth(Branch(10, Branch(20,
                    Empty,
                    Empty
                 ), Branch(30,
                    Branch(40,
                        Empty,
                        Empty
                    ),
                 Empty))))
                 
// def sort(tree: Tree): Tree = tree match{
//   case Branch(v, l ,r) => {
//     if(v < max(l)) {
//       sort(l)
//     }
// }


//�����֐�
List(1, 2, 3, 4, 5).collect { case i if i % 2 == 1 => i * 2 }