//���K���
def swapAray[T](arr: Array[T])(i: Int, j:Int): Unit = {
  val tmp = arr(i)
  arr(i) = arr(j)
  arr(j) = tmp
}
var arr  = Array(1,2,3,4)

swapAray(arr)(1,2)
println(arr(1))

val a1 = 1 :: Nil
println(a1)

val a2 = 2 :: a1
println(a2)

//���K���2
def joinByComma(start: Int, end: Int): String = {
  (start to end).mkString(",")
}

println(joinByComma(1,5))

List(1,2,3).foldLeft(1)((x,y) => x*y)
println(List(1,2,3).foldLeft(1)((x,y) => x*y))

//���K���3
def reverse[T](list: List[T]): List[T] = {
  list.foldLeft[List[T]](Nil)((x, y) => y :: x)
}

println(reverse(List(1,2,3,4)))

//���K���4
def sum(list: List[Int]): Int = list.foldRight(0)((a,b) => a+b)

println(sum(List(1,2,3)))
//���K���5
def mul(list: List[Int]): Int = list.foldRight(1)((a,b) => a*b)

println(mul(List(1,2,3)))

//���K���6
def mkString[T](list: List[T])(sep: String): String = {
  list.foldLeft[String]("")((a,b) => {
    if (a == ""){
      b.toString
    }else{
      a.toString +sep + b.toString
    }
  }
  )
}

println(mkString[Int](List(1,3,4))("b"))

//���K���7
def map[T, U](list: List[T])(f: T => U): List[U] = {
  list.foldLeft(Nil: List[U])((x,y) => f(y) :: x).reverse
}

println(map(List(2,3,4))((x) => x+1))

//���K���8
def filter[T](list: List[T])(f: T => Boolean): List[T] = {
  list.foldLeft(Nil: List[T])((x,y) => f(y) match {
    case true => y :: x
    case false => x}).reverse
}

println(filter(List(1,2,3,4,5))(x => x % 2 == 1))

//���K���9
def count[T](list: List[T])(f: T => Boolean): Int = {
  list.foldLeft(0)((x,y) => if(f(y)) x+1 else x)
}

println(count(List(1,2,3,4,5))(x => x % 2 == 1))