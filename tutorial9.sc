implicit def intToBoolean(arg: Int): Boolean = arg != 0
if(1){
  println("true")
}
var s : String = "33"
s.toInt
//���K���
implicit def stringToInt(arg: String) = arg.toInt
val n : Int = "3"
//���K���2
//def max[B >: A](implicit ord: math.Ordering[B]): T

//���K���3
case class Point(x: Int, y: Int)

trait Additive[T]{
  def plus(a:T, b:T): T
  def zero(): T
}

implicit object PointAdditive extends Additive[Point]{
  def plus(a: Point, b: Point) = Point(a.x + b.x, a.y + b.y)
  def zero() = Point(0, 0)
}

def sum[A](lst: List[A])(implicit m: Additive[A]) = lst.foldLeft(m.zero)((x, y) => m.plus(x, y))

sum(List(Point(1,3), Point(2,4)))

//���K���4
//math.Numeric[B]
