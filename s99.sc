
//P01
def last[A](list: List[A]): A = list(list.size-1)

last(List(1, 1, 2, 3, 5, 8))

//P02
def penultimate[A](ints: List[A]) = ints(ints.size-2)
penultimate(List(1, 1, 2, 3, 5, 8))

//P03
def nth[A](n:Int, list: List[A]): A = list(n)
nth(2, List(1, 1, 2, 3, 5, 8))

//P04
def length[A](ints: List[A]): Int = ints.size
length(List(1, 1, 2, 3, 5, 8))

//P05
def reverse[A](list: List[A]):List[A] = list.foldLeft[List[A]](Nil)((x, y) =>
  y :: x
)
reverse(List(1, 1, 2, 3, 5, 8))

//P06
def isPalindrome[A](ints: List[A]): Boolean = {
  if(ints == reverse(ints)) true else false
}
isPalindrome(List(1, 2, 3, 2, 1))

//P07
def flatten(list: List[Any]): List[Any] = list match {
  case (x: List[_]) :: xs => flatten(x) ::: flatten(xs)
  case x :: xs => x :: flatten(xs)
  case Nil => Nil
}
flatten(List(List(1, 1), 2, List(3, List(5, 8))))

List(1,2) :: List(1, 3)

//P08
def compress[A](list: List[A]): List[A] = {
  def compress1(pre: A, rest: List[A]): List[A] = rest match {
    case x :: xs if pre == x => compress1(pre, xs)
    case x :: xs             => pre :: compress1(x, xs)
    case Nil                 => pre :: Nil
  }
  list match {
    case x :: xs => compress1(x, xs)
    case Nil     => Nil
  }
}
compress(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e))

//P09
def pack[T](list: List[T]): List[List[T]] =
  list.foldRight(Nil: List[List[T]]) { (c, ls) =>
    ls match {
      case (x @ `c` :: _) :: xs => (c :: x) :: xs
      case _                    => List(c) :: ls
    }
  }


pack(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e))

//P10
def encode[T](list: List[T]): List[(Int, T)] ={
  pack(list).foldRight(Nil: List[(Int, T)]){(k, c) =>
    (k.size, k(0)) :: c
  }
  }
encode(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e))

//P11
def encodeModified[T](list: List[T]): List[Any] ={
  encode(list).foldRight(Nil: List[Any]){(x, y) => x match{
    case (1, b) => b :: y
    case _ => x :: y
  }}
}
encodeModified(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e))

//P12
def decode[T](list: List[(Int, T)]): List[T] = {
  def decode1[T](i: Int, a: List[T], t: T): List[T] ={
    if (i > 0) decode1(i-1, t :: a, t) else a
  }
  list.foldRight(Nil: List[(T)]){
    (x,y) => x match {
      case ((a: Int), (b: T)) =>  decode1(a, Nil, b) ::: y
    }
  }
}
decode(List((4, 'a), (1, 'b), (2, 'c), (2, 'a), (1, 'd), (4, 'e)))