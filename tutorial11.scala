import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Future, Promise}
import scala.util.{Failure, Random, Success}
import java.util.concurrent.atomic.AtomicInteger

object CountDownLatch extends App {
  val promiseGetInt: Promise[Int] = Promise[Int]
  val futureByPromise: Future[Int] = promiseGetInt.future
  val offset = new AtomicInteger(0)
  val promises: Seq[Promise[Int]] = for { i <- 1 to 3} yield Promise[Int]

  val random = new Random()
  def waitRandom(): Int ={
    val waitMilliSec = random.nextInt(1000)
    Thread.sleep(waitMilliSec)
    waitMilliSec
  }
  val futures: Seq[Future[Unit]] = for {i <- 1 to 8} yield Future[Unit]{
    val waitMillSec = waitRandom
    val index = offset.getAndIncrement()
    if(index < promises.length) {
      println(i)
      promises(index).success(waitMillSec)
      println(s"[ThreadName] In Future: ${Thread.currentThread.getName}")
      println(waitMillSec)
    }
  }

//  futures.foreach{ f => f.foreach {case waitMillSec =>
//      val index = offset.getAndIncrement()
//      if(index < promises.length) {
//        println("hidouki")
//        promises(index).success(waitMillSec)
//        println(s"[ThreadName] In Future: ${Thread.currentThread.getName}")
//      }
//  }}
//  promises.foreach{p => p.future.foreach{case waitMillSec => println(waitMillSec)}}
  Thread.sleep(5000)
}