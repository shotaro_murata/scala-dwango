import java.util.HashMap
import java.lang.System

import scala.collection.mutable.ArrayBuffer

//���K���
val set: HashMap[String, Int] = new HashMap[String, Int]

//���K���2
System.out.println("Hello, World!")

//���K���3
//System.exit(0)

//���K���4
//System.out.println("Hello, World!")

set.put("A", 1)
set.put("B", 2)

Option(set.get("A"))
Option(set.get("D"))

import scala.collection.JavaConverters._
import java.util.ArrayList

val list = new ArrayList[String]()

list.add("A")

list.add("B")

val scalaList = list.asScala

//���K���5
val slist = new ArrayBuffer[String]

slist.append("add")
slist.append("remove")

val jlist = slist.asJava


import java.util.{List => JList, ArrayList => JArrayList}
val objects: JList[_ <: Object] = new JArrayList[String]()

import java.util.{Comparator => JComparator}
val cmp: JComparator[_ >: String] = new JComparator[Any] {
  override def compare (o1: Any, o2: Any): Int ={
    o1.hashCode() - o2.hashCode()
  }
}
