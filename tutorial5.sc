val add = new Function2[Int, Int, Int]{
  def apply(x: Int, y: Int): Int = x + y
}

println(add)
println(add.apply(10,200))
//syntax suger
val add2 = (x: Int, y: Int) => x + y
println(add2.apply(10,20))
//curried
val add3 = (x: Int) => ((y: Int) => x + y)
println(add3.apply(10)(20))
//高かい関数
def double(n: Int, f: Int => Int): Int = {
  f(f(n))
}
println(double(2, (x: Int) => x*x))

def around(init: () => Unit, body: () => Any, fin: () => Unit): Any = {
  init()
     try {
       body()
     } finally {
       fin()
       
     }
  
}

around(
        () => println("ファイルを開く"),
        () => println("ファイルに対する処理"),
        () => println("ファイルを閉じる") )
        
//練習問題
import scala.io.Source

def withFile[A](filename: String)(f: Source => A): A = {
  val s = Source.fromFile(filename)
  try {
    f(s)
  } finally {
    s.close()
  }
}

def printFile(filename: String): Unit = {
  withFile(filename){file =>
    file.getLines.foreach(println)
  }
}

printFile("/tutorial1.sc")