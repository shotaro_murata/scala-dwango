import org.scalatest._
import org.scalatest.concurrent.TimeLimits
import org.scalatest.time.SpanSugar._
import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._

class ClacSpec extends FlatSpec with DiagrammedAssertions with TimeLimits with MockitoSugar{
  val calc = new Calc

  "sum function" should "get array of Integer and return sum of these value." in {
    assert(calc.sum(Seq(1, 2, 3)) === 6)
    assert(calc.sum(Seq(0)) === 0)
    assert(calc.sum(Seq(-1, 1)) === 0)
    assert(calc.sum(Seq()) === 0)
  }

  it should "overflow when value is behind Int max." in {
    assert(calc.sum(Seq(Integer.MAX_VALUE, 1)) === Integer.MIN_VALUE)
  }

  "div function" should "get two Int values and return divided value the numerator by the denominator in float." in {
    assert(calc.div(6, 3) === 2.0)
    assert(calc.div(1, 3) === 0.3333333333333333)
  }

  it should "throw RuntimeError when divided by zero." in{
    intercept[ArithmeticException] {
      calc.div(1, 0)
    }
  }

  "isPrime function" should "return in boolean whether argment is prime value." in {
    assert(calc.isPrime(0) === false)
    assert(calc.isPrime(-1) === false)
    assert(calc.isPrime(2))
    assert(calc.isPrime(17))
  }

  it should "calc value under 10 million in 1 sec." in {
    failAfter(1000 millis) {
      assert(calc.isPrime(9999991))
    }
  }

  "Calc mock object" should "do fake behavor" in {
    val mockCalc = mock[Calc]
    when(mockCalc.sum(Seq(3, 4, 5))).thenReturn(12)
    assert(mockCalc.sum(Seq(3, 4, 5)) === 12)
  }

}