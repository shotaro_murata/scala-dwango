class Calc {
  def sum(seq: Seq[Int]): Int = seq.foldLeft(0)(_ + _)

  def div(a: Int, b: Int): Double = {
    if (b == 0) throw new ArithmeticException("/by zero")
    a.toDouble / b.toDouble
  }

  def isPrime(a: Int): Boolean = {
    var out = false
    if(a >= 2) {
      out = true
      for (i <- 2 to a / 2) {
        if (a % i == 0) {
          false
        }
      }
    }
    out
  }

}