class Point3D(val x : Int, val y : Int, val z: Int){
  
}

val p = new Point3D(10,20,30)
println(p.x)
println(p.y)
println(p.z)

class Point(x_ :Int, y_ :Int){
  private val x = x_
  private val y = y_
}

object Point {
  def printX(): Unit = {
    val xy = new Point(10, 20)
    println(xy.x)
  }
  
}

Point.printX()