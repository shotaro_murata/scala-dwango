
  import java.security._

  var age: Int = 5
  var isSchoolStarted: Boolean = false



  if (age <= 6 && age >= 1 || !isSchoolStarted){
    println("幼児です")
  }else{
    println("幼児ではありません")
  }

  def loopFrom0to9(): Unit = {
    var i = 0
    do {
      println(i)
      i += 1
    }while(i < 10)
  }

  loopFrom0to9()

  for (a <- 1 to 1000){
    for(b <- 1 to 1000){
      if (a*a + b*b <= 1000){
        println((a, b, a*a + b*b))
      }
    }
  }


  // val obj: AnyRef = "String Literal"
  // obj match {
  //         case v:java.lang.Integer =>
  //           println("Integer!")
  //         case v:String =>
  //           println(v.toUpperCase(Locale.ENGLISH))

  // }

  // for(i <- 1 to 1000) {
  //   val s = new scala.util.Random(new java.security.SecureRandom()).alphanumeric.take(5).toList match {
  //     case List(a,b,c,d,_) => List(a,b,c,d,a).mkString
  //   }
  //   println(s)
  // }
  // $FiddleEnd
